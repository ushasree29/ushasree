create database capstone_grp6;
use capstone_grp6;
--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminid` int NOT NULL AUTO_INCREMENT,
  `adminname` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`adminid`)
) ;
--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
INSERT INTO `admin` VALUES (1,'admin','admin@hcl.com','1234');
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `answerimage` text,
  `answername` text NOT NULL,
  `astatus` int NOT NULL,
  `question_id` int NOT NULL,
  `user_userid` int NOT NULL,
  PRIMARY KEY (`id`)
 /* KEY `question_id_idx` (`question_id`) /*!80000 INVISIBLE */
  /*KEY `user_userid_idx` (`user_userid`) /*!80000 INVISIBLE */
  /*CONSTRAINT `question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)*/
  /*CONSTRAINT `userid` FOREIGN KEY (`user_userid`) REFERENCES `users` (`userid`)*/
);
--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
INSERT INTO `answer` VALUES (2,'no image','amswer 1',0,1,1),(3,'no image','amswer 2',0,4,2),(4,NULL,'qwertyy',1,1,4);
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int NOT NULL AUTO_INCREMENT,
  `qstatus` int NOT NULL,
  `questionimage` text,
  `questionname` text NOT NULL,
  `topic_topicid` int NOT NULL,
  `user_userid` int NOT NULL,
  PRIMARY KEY (`id`)
 /* KEY `topic_topicid_idx` (`topic_topicid`)*/
  /*KEY `user_userid_idx` (`user_userid`)*/
  /*CONSTRAINT `FK17wht8h89n15j80ae8vt8jo86` FOREIGN KEY (`user_userid`) REFERENCES `users` (`userid`)*/
/*CONSTRAINT `FKm9cahdnp5jxl2gwvm32iyig4w` FOREIGN KEY (`topic_topicid`) REFERENCES `topic` (`topicid`)*/
) ;
--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
INSERT INTO `question` VALUES (1,0,'image url','answer 1',1,1),(4,0,'image','Question 4',1,1),(8,1,'questionimage1','What Is Java',1,1),(9,1,'questionimage1','What Is Java',4,2),(10,1,'questionimage1','What Is Spring',5,4),(11,1,'questionimage1','How old is Kannada language',8,4),(12,1,'questionimage1','How old is Kannada language',8,4),(13,1,'questionimage1','How old is Kannada language',8,1);
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `topicid` int NOT NULL AUTO_INCREMENT,
  `cetegory` varchar(255) NOT NULL,
  PRIMARY KEY (`topicid`)
) ;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
INSERT INTO `topic` VALUES (1,'Science '),(2,'Maths'),(3,'Physics'),(4,'Java'),(5,'Spring'),(6,'Angular'),(7,'Python'),(8,'Kannada'),(9,'EEE'),(10,'Hindi'),(11,'telugu'),(12,'android');
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` int NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `admin_adminid` int NOT NULL,
  PRIMARY KEY (`userid`)
) ;
--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'email2','name2','password2',2),(2,'email5','name','password',3);
UNLOCK TABLES;
