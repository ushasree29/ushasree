package com.doconnect.springboot.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="answer")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int answer_id;
	@Column
	private String answer;
	
//	@ManyToOne(fetch = FetchType.LAZY, optional = false)
//    
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    private user user2;
//	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Question question;
	
	
	public int getAnswer_id() {
		return answer_id;
	}
	public String getAnswer() {
		return answer;
	}
	
	public void setAnswer_id(int answer_id) {
		this.answer_id = answer_id;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
	
    
    
	
	
	
	

	



	
		
	}