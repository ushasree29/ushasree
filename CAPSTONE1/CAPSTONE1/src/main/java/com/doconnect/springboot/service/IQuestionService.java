package com.doconnect.springboot.service;

import java.util.List;

import com.doconnect.springboot.beans.Question;
import com.doconnect.springboot.exception.ProjectException;

public interface IQuestionService {



	public Question addQuestion(Question question);

	public List<Question> getAllQuestion();

	public Question getUserById(Integer questionId) throws ProjectException;

	public Question updateQuestion(Question question);

	public String deleteQuestionById(Integer questionId) throws ProjectException;

	Question getQuestionById(Integer questionId) throws ProjectException;
	
	public List<Question> getQuestionsByUserId(int userId);
	

	

}