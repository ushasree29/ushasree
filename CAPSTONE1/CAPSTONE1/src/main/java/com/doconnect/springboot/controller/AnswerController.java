package com.doconnect.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doconnect.springboot.beans.Answer;
import com.doconnect.springboot.beans.ResponseMessage;
import com.doconnect.springboot.exception.ProjectException;
import com.doconnect.springboot.service.IAnswerService;

@RestController
public class AnswerController {

	@Autowired
	IAnswerService AnswerService;

	// Register User
	@RequestMapping(method = RequestMethod.POST, value = "/AnswerRegister")
	public ResponseEntity<Answer> addAnswer(@RequestBody Answer Answer) {

		return new ResponseEntity<Answer>(AnswerService.addAnswer(Answer), HttpStatus.CREATED);
	}

	// Get All Users
	@RequestMapping(method = RequestMethod.GET, value = "/getAllAnswer")
	public ResponseEntity<List<Answer>> getAllAnswer() {

		return new ResponseEntity<List<Answer>>(AnswerService.getAllAnswer(), HttpStatus.OK);
	}

	// Get User By ID
	@RequestMapping(method = RequestMethod.GET, value = "/getAnswerById/{id}")
	public ResponseEntity<Answer> getUserById(@PathVariable("id") Integer id) throws ProjectException {

		return new ResponseEntity<Answer>(AnswerService.getAnswerById(id), HttpStatus.OK);
	}

	// Update User
	@RequestMapping(method = RequestMethod.PUT, value = "/updateAnswer")
	public ResponseEntity<Answer> updateUser(@RequestBody Answer Answer) {

		return new ResponseEntity<Answer>(AnswerService.updateAnswer(Answer), HttpStatus.OK);
	}

	// Delete User
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteAnswerById/{id}")
	public ResponseEntity<ResponseMessage> deleteAnswerById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(AnswerService.deleteAnswerById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}