package com.doconnect.springboot.beans;




import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.*;

@Entity(name="admin")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Admin {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String admin_email;
	private String password;
	
//	@OneToMany(mappedBy = "admin", cascade = CascadeType.  )
//  private List<user> question;
	
	@OneToMany(mappedBy = "admin", cascade = CascadeType.ALL)
	@JsonIgnore
    private List<Question> questions;

	
	
	public int getId() {
		return id;
	}
	public String getAdmin_email() {
		return admin_email;
	}
	public String getPassword() {
		return password;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	



	
		
	}