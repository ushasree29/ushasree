package com.doconnect.springboot.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="question")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int question_id;
	@Column
	private String question;
	@Column
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getQuestion_id() {
		return question_id;
	}
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private user user;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Admin admin;
	
	@OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<Answer> answers;

	
	
	
//	@ManyToOne(fetch = FetchType.EAGER, optional = false)
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    private com.doconnect.springboot.beans.user user;
	
    
	

	
		
	}