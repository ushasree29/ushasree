package com.doconnect.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doconnect.springboot.beans.ResponseMessage;
import com.doconnect.springboot.beans.user;
import com.doconnect.springboot.exception.ProjectException;
import com.doconnect.springboot.service.IUserService;

@RestController
public class UserController {

	@Autowired
	IUserService userService;

	// Register User
	@RequestMapping(method = RequestMethod.POST, value = "/UserRegister")
	public ResponseEntity<user> addUser(@RequestBody user user) {

		return new ResponseEntity<user>(userService.addUser(user), HttpStatus.CREATED);
	}

	// Get All Users
	@RequestMapping(method = RequestMethod.GET, value = "/getAllUsers")
	public ResponseEntity<List<user>> getAllUsers() {

		return new ResponseEntity<List<user>>(userService.getAllUsers(), HttpStatus.OK);
	}

	// Get User By ID
	@RequestMapping(method = RequestMethod.GET, value = "/getUserById/{id}")
	public ResponseEntity<user> getUserById(@PathVariable("id") Integer id) throws ProjectException {

		return new ResponseEntity<user>(userService.getUserById(id), HttpStatus.OK);
	}

	// Update User
	@RequestMapping(method = RequestMethod.PUT, value = "/updateUser")
	public ResponseEntity<user> updateUser(@RequestBody user user) {

		return new ResponseEntity<user>(userService.updateUser(user), HttpStatus.OK);
	}

	// Delete User
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteUserById/{id}")
	public ResponseEntity<ResponseMessage> deleteUserById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(userService.deleteUserById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	@GetMapping("/find/{email}/{password}")
	public ResponseEntity<user> getUserByemailandpassword(@PathVariable("email") String email, @PathVariable("password") String password){
		
		user user = this.userService.findUserDetailsByEmailAndPassword(email, password);
		return new ResponseEntity<>(user, HttpStatus.OK);
		
	}

	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}