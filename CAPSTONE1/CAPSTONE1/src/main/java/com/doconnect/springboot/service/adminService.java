package com.doconnect.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doconnect.springboot.beans.Admin;
import com.doconnect.springboot.beans.user;
import com.doconnect.springboot.dao.IAdminRepository;
import com.doconnect.springboot.exception.ProjectException;

@Service
public class adminService implements IAdminService {

	@Autowired
	IAdminRepository adminRepository;

	@Override
	public Admin addAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminRepository.save(admin);
	}

	@Override
	public List<Admin> getAllAdmin() {
		// TODO Auto-generated method stub
		return adminRepository.findAll();
	}

	@Override
	public Admin getUserById(Integer adminId) throws ProjectException {
		// TODO Auto-generated method stub
		return adminRepository.findById(adminId).orElseThrow(() -> new ProjectException("admin Id not found"));
	}

	@Override
	public Admin updateAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminRepository.saveAndFlush(admin);
	}

	@Override
	public String deleteAdminById(Integer adminId) throws ProjectException {
		// TODO Auto-generated method stub
		Admin admin = adminRepository.findById(adminId).orElseThrow(() -> new ProjectException("admin Id not found"));
		adminRepository.delete(admin);
		return "Deleted Successfully";
	}

	@Override
	public Admin getAdminById(Integer adminId) throws ProjectException {
		// TODO Auto-generated method stub
		return adminRepository.findById(adminId).orElseThrow(() -> new ProjectException("admin Id not found"));
	}
	@Override
	public Admin findAdminDetailsByEmailAndPassword(String admin_email, String password) {
		// TODO Auto-generated method stub
		System.out.println(admin_email+" "+password);
		return adminRepository.findByAdmin_emailAndPassword(admin_email, password);
	}

//	@Override
//	public Admin addAdmin(Admin admin) {
//		// TODO Auto-generated method stub
//		return adminRepository.save(admin);
//	}
//
//	@Override
//	public List<Admin> getAllAdmin() {
//		// TODO Auto-generated method stub
//		return adminRepository.findAll();
//	}
//
//	@Override
//	public Admin getAdminById(Integer adminId) throws ProjectException {
//		// TODO Auto-generated method stub
//		return adminRepository.findById(adminId).orElseThrow(() -> new ProjectException("admin Id not found"));
//	}
//
//	@Override
//	public Admin updateAdmin(Admin admin) {
//		// TODO Auto-generated method stub
//		return adminRepository.saveAndFlush(admin);
//	}
//
//	@Override
//	public String deleteAdminById(Integer adminId) throws ProjectException {
//		// TODO Auto-generated method stub
//		Admin admin = adminRepository.findById(adminId).orElseThrow(() -> new ProjectException("admin Id not found"));
//		adminRepository.delete(admin);
//		return "Deleted Successfully";
//	}
//
//	@Override
//	public Admin addAdmin(Admin admin) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Admin> getAllAdmin() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Admin getAdminById(Integer adminId) throws ProjectException {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Admin updateAdmin(Admin admin) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String deleteAdminById(Integer adminId) throws ProjectException {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Admin getUserById(Integer adminId) throws ProjectException {
//		// TODO Auto-generated method stub
//		return null;
//	}

}