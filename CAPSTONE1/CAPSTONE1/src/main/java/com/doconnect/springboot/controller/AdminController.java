package com.doconnect.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doconnect.springboot.beans.Admin;
import com.doconnect.springboot.beans.ResponseMessage;
import com.doconnect.springboot.beans.user;
import com.doconnect.springboot.exception.ProjectException;
import com.doconnect.springboot.service.IAdminService;

@RestController
public class AdminController {

	@Autowired
	IAdminService adminService;

	// Register User
	@RequestMapping(method = RequestMethod.POST, value = "/AdminRegister")
	public ResponseEntity<Admin> addAdmin(@RequestBody Admin admin) {

		return new ResponseEntity<Admin>(adminService.addAdmin(admin), HttpStatus.CREATED);
	}

	// Get All Users
	@RequestMapping(method = RequestMethod.GET, value = "/getAllAdmin")
	public ResponseEntity<List<Admin>> getAllAdmin() {

		return new ResponseEntity<List<Admin>>(adminService.getAllAdmin(), HttpStatus.OK);
	}

	// Get User By ID
	@RequestMapping(method = RequestMethod.GET, value = "/getAdminById/{id}")
	public ResponseEntity<Admin> getUserById(@PathVariable("id") Integer id) throws ProjectException {

		return new ResponseEntity<Admin>(adminService.getAdminById(id), HttpStatus.OK);
	}

	// Update User
	@RequestMapping(method = RequestMethod.PUT, value = "/updateAdmin")
	public ResponseEntity<Admin> updateUser(@RequestBody Admin admin) {

		return new ResponseEntity<Admin>(adminService.updateAdmin(admin), HttpStatus.OK);
	}

	// Delete User
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteAdminById/{id}")
	public ResponseEntity<ResponseMessage> deleteAdminById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(adminService.deleteAdminById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}
	
	@GetMapping("/FindAdmin/{admin_email}/{password}")
	public ResponseEntity<Admin> getAdminByemailandpassword(@PathVariable("admin_email") String admin_email, @PathVariable("password") String password){
		
		Admin admin = this.adminService.findAdminDetailsByEmailAndPassword(admin_email, password);
		return new ResponseEntity<>(admin, HttpStatus.OK);
		
	}

	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}
