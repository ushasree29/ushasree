package com.doconnect.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doconnect.springboot.beans.user;
import com.doconnect.springboot.dao.IUserRepository;
import com.doconnect.springboot.exception.ProjectException;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	IUserRepository userRepository;

	@Override
	public user addUser(user user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public List<user> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public user getUserById(Integer userId) throws ProjectException {
		// TODO Auto-generated method stub
		return userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
	}

	@Override
	public user updateUser(user user) {
		// TODO Auto-generated method stub
		return userRepository.saveAndFlush(user);
	}

	@Override
	public String deleteUserById(Integer userId) throws ProjectException {
		// TODO Auto-generated method stub
		user user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		userRepository.delete(user);
		return "Deleted Successfully";
	}

	@Override
	public user findUserDetailsByEmailAndPassword(String email, String password) {
		// TODO Auto-generated method stub
		return userRepository.findUserByEmailAndPassword(email, password);
	}

}