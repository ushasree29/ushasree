package com.doconnect.springboot.service;

import java.util.List;

import com.doconnect.springboot.beans.Answer;
import com.doconnect.springboot.exception.ProjectException;

public interface IAnswerService {



	public Answer addAnswer(Answer answer);

	public List<Answer> getAllAnswer();

	public Answer getUserById(Integer answerId) throws ProjectException;

	public Answer updateAnswer(Answer answer);

	public String deleteAnswerById(Integer answerId) throws ProjectException;

	Answer getAnswerById(Integer answerId) throws ProjectException;

	

}