package com.doconnect.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doconnect.springboot.beans.Admin;
import com.doconnect.springboot.beans.Question;
import com.doconnect.springboot.beans.ResponseMessage;
import com.doconnect.springboot.exception.ProjectException;
import com.doconnect.springboot.service.IQuestionService;
import com.doconnect.springboot.service.adminService;

@RestController
public class QuestionController {

	@Autowired
	IQuestionService QuestionService;

	// Register User
	@RequestMapping(method = RequestMethod.POST, value = "/QuestionRegister")
	public ResponseEntity<Question> addQuestion(@RequestBody Question question) {
		System.out.println("error here "+question);
//		try {
//			Admin admin = new adminService().getUserById(1);
//			question.setAdmin(admin);
//			return new ResponseEntity<Question>(QuestionService.addQuestion(question), HttpStatus.CREATED);
//		} catch (ProjectException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return new ResponseEntity<Question>(QuestionService.addQuestion(question), HttpStatus.CREATED);
	}

	// Get All Users
	@RequestMapping(method = RequestMethod.GET, value = "/getAllQuestion")
	public ResponseEntity<List<Question>> getAllQuestion() {

		return new ResponseEntity<List<Question>>(QuestionService.getAllQuestion(), HttpStatus.OK);
	}

	// Get User By ID
	@RequestMapping(method = RequestMethod.GET, value = "/getQuestionById/{id}")
	public ResponseEntity<Question> getUserById(@PathVariable("id") Integer id) throws ProjectException {

		return new ResponseEntity<Question>(QuestionService.getQuestionById(id), HttpStatus.OK);
	}

	// Update User
	@RequestMapping(method = RequestMethod.PUT, value = "/updateQuestion")
	public ResponseEntity<Question> updateUser(@RequestBody Question Question) {

		return new ResponseEntity<Question>(QuestionService.updateQuestion(Question), HttpStatus.OK);
	}

	// Delete User
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteQuestionById/{id}")
	public ResponseEntity<ResponseMessage> deleteQuestionById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(QuestionService.deleteQuestionById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@GetMapping("/userId/{id}")
	public ResponseEntity<List<Question>> getQuestionsByUserId(@PathVariable("id") int id)
	{
		
		
		return new ResponseEntity<List<Question>>(QuestionService.getQuestionsByUserId(id), HttpStatus.OK);
	}
	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}