package com.doconnect.springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.doconnect.springboot.beans.Answer;

@Repository
public interface IAnswerRepository extends JpaRepository<Answer, Integer>{

}
