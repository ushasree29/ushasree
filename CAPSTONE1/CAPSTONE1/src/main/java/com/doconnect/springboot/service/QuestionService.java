package com.doconnect.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doconnect.springboot.beans.Question;
import com.doconnect.springboot.dao.IQuestionRepository;
import com.doconnect.springboot.exception.ProjectException;

@Service
public class QuestionService implements IQuestionService {

	@Autowired
	IQuestionRepository questionRepository;

	@Override
	public Question addQuestion(Question question) {
		// TODO Auto-generated method stub
		return questionRepository.save(question);
	}

	@Override
	public List<Question> getAllQuestion() {
		// TODO Auto-generated method stub
		return questionRepository.findAll();
	}

	@Override
	public Question getUserById(Integer questionId) throws ProjectException {
		// TODO Auto-generated method stub
		return questionRepository.findById(questionId).orElseThrow(() -> new ProjectException("question Id not found"));
	}

	@Override
	public Question updateQuestion(Question question) {
		// TODO Auto-generated method stub
		return questionRepository.saveAndFlush(question);
	}

	@Override
	public String deleteQuestionById(Integer questionId) throws ProjectException {
		// TODO Auto-generated method stub
		Question question = questionRepository.findById(questionId).orElseThrow(() -> new ProjectException("question Id not found"));
		questionRepository.delete(question);
		return "Deleted Successfully";
	}

	@Override
	public Question getQuestionById(Integer questionId) throws ProjectException {
		// TODO Auto-generated method stub
		return questionRepository.findById(questionId).orElseThrow(() -> new ProjectException("question Id not found"));
	}
	public List<Question> getQuestionsByUserId(int userId)
	{
		return questionRepository.findByUserId(userId);
	}
}