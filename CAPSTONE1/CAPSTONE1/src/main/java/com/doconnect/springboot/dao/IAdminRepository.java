package com.doconnect.springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.doconnect.springboot.beans.Admin;

@Repository
public interface IAdminRepository extends JpaRepository<Admin, Integer>{

	@Query(value="select * from Admin where admin_email = :admin_email and password = :password", nativeQuery=true)
	Admin findByAdmin_emailAndPassword(String admin_email, String password);

}
