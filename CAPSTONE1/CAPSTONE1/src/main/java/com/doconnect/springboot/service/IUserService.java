package com.doconnect.springboot.service;

import java.util.List;

import com.doconnect.springboot.beans.user;
import com.doconnect.springboot.exception.ProjectException;

public interface IUserService {

	public user addUser(user user);

	public List<user> getAllUsers();

	public user getUserById(Integer userId) throws ProjectException;

	public user updateUser(user user);

	public String deleteUserById(Integer userId) throws ProjectException;

	public user findUserDetailsByEmailAndPassword(String email, String password);

}
