package com.doconnect.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doconnect.springboot.beans.Answer;
import com.doconnect.springboot.dao.IAnswerRepository;
import com.doconnect.springboot.exception.ProjectException;

@Service
public class AnswerService implements IAnswerService {

	@Autowired
	IAnswerRepository answerRepository;

	@Override
	public Answer addAnswer(Answer answer) {
		// TODO Auto-generated method stub
		return answerRepository.save(answer);
	}

	@Override
	public List<Answer> getAllAnswer() {
		// TODO Auto-generated method stub
		return answerRepository.findAll();
	}

	@Override
	public Answer getAnswerById(Integer answerId) throws ProjectException {
		// TODO Auto-generated method stub
		return answerRepository.findById(answerId).orElseThrow(() -> new ProjectException("answer Id not found"));
	}

	@Override
	public Answer updateAnswer(Answer answer) {
		// TODO Auto-generated method stub
		return answerRepository.saveAndFlush(answer);
	}

	@Override
	public String deleteAnswerById(Integer answerId) throws ProjectException {
		// TODO Auto-generated method stub
		Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new ProjectException("answer Id not found"));
		answerRepository.delete(answer);
		return "Deleted Successfully";
	}

	@Override
	public Answer getUserById(Integer answerId) throws ProjectException {
		// TODO Auto-generated method stub
		return null;
	}


	
}