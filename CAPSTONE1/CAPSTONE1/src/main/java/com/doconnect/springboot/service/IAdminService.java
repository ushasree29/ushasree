package com.doconnect.springboot.service;

import java.util.List;

import com.doconnect.springboot.beans.Admin;
import com.doconnect.springboot.exception.ProjectException;

public interface IAdminService {



	public Admin addAdmin(Admin admin);

	public List<Admin> getAllAdmin();

	public Admin getUserById(Integer adminId) throws ProjectException;

	public Admin updateAdmin(Admin admin);

	public String deleteAdminById(Integer adminId) throws ProjectException;

	public Admin getAdminById(Integer adminId) throws ProjectException;

	public Admin findAdminDetailsByEmailAndPassword(String admin_email, String password);

	

}
