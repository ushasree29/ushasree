package com.doconnect.springboot.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.doconnect.springboot.beans.Question;

@Repository
public interface IQuestionRepository extends JpaRepository<Question, Integer>{
	
	public List<Question> findByUserId(int user_id);
	
	
}
