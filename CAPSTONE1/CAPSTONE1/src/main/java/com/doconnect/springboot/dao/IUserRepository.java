package com.doconnect.springboot.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.doconnect.springboot.beans.user;

@Repository
public interface IUserRepository extends JpaRepository<user, Integer>{

user	findUserByEmailAndPassword(String email, String Password);
}
