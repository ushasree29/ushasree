package com.greatlearning.capstone6.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone6.bean.Admin;

@Repository
public interface AdminDao extends JpaRepository<Admin,String>{

}
