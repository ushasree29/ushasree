package com.greatlearning.capstone6.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.greatlearning.capstone6")
@EntityScan(basePackages = "com.greatlearning.capstone6.bean")
@EnableJpaRepositories(basePackages = "com.greatlearning.capstone6.dao")
@EnableSwagger2
@EnableEurekaClient
public class DoConnectAdminServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoConnectAdminServiceApplication.class, args);
		System.out.println("client running on port no : 8585");
	}
	
	@Bean						// container will call this method when string boot load the application. 
	public Docket api() {
		System.out.println("object created..");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com")).build();
	}

}
