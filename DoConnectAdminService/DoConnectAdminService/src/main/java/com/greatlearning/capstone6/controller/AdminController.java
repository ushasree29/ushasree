package com.greatlearning.capstone6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.greatlearning.capstone6.bean.Admin;
import com.greatlearning.capstone6.service.AdminService;


@RestController
@RequestMapping(value="/admin")
public class AdminController {

	@Autowired
	AdminService adminService;
	
	
	 
	 @GetMapping(value="getAllAdmin",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Admin> getAllAdminLogin(){
		 return adminService.getAllAdminInfo();
	 }
	 
	 @PostMapping(value="storeAdmin",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeAdminLogin(@RequestBody Admin ad) {
		 return adminService.storeAdminInfo(ad);
	 }
	 
	 @DeleteMapping(value="deleteAdmin/{email}")
	 public String deleteAdmin(@PathVariable("email")String email) {
		 return adminService.deleteAdminInfo(email);
	 }
	 
	 @PatchMapping(value="updateAdmin")
	 public String updateAdmin(@RequestBody Admin ad) {
		 return adminService.updateAdminInfo(ad);
	 }
	 
	 
	 
	 
}


//@GetMapping(value="logout",produces = MediaType.APPLICATION_JSON_VALUE)
//public String logoutAdmin(@PathVariable("email")String email){
//	 return adminService.logoutInfo(email);
//}
