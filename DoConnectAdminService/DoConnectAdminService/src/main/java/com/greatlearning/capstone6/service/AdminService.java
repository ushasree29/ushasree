package com.greatlearning.capstone6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.capstone6.bean.Admin;
import com.greatlearning.capstone6.dao.AdminDao;



@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;
	
	public List<Admin> getAllAdminInfo(){
	  	  return adminDao.findAll();
	}
	    
	public String storeAdminInfo(Admin ad) {
	if(adminDao.existsById(ad.getEmail())) {
	  		  return "enter valid emailid";
	}else {
	  		adminDao.save(ad);
	  		  return "admin information saved successfully";
	      }
	}
	
	public String deleteAdminInfo(String email) {
		if(!adminDao.existsById(email)) {
			return "email id not present";
		}else {
			adminDao.deleteById(email);
			return "email deleted successfully";
		}
	}
	
	public String updateAdminInfo(Admin ad) {
		if(!adminDao.existsById(ad.getEmail())) {
			return "email id not present";
		}else {
			Admin admin=adminDao.getById(ad.getEmail());
			admin.setPassword(ad.getPassword());
			adminDao.saveAndFlush(admin);
			return "password updated successfully";
		}
	}
	
	public String logoutInfo(String email) {
		if(!adminDao.existsById(email)) {
			return "email not present";
		}else {
			return "logged out successfully";
		}
	}
}
