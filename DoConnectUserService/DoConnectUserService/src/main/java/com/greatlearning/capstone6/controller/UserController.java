package com.greatlearning.capstone6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.greatlearning.capstone6.bean.User;
import com.greatlearning.capstone6.service.UserService;

@RestController
@RequestMapping(value="/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping(value="getAllUser",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<User> getAllUser(){
		 return userService.getAllUserInfo();
	 }
	 
	 @PostMapping(value="storeUser",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeUser(@RequestBody User u) {
		 return userService.storeUserInfo(u);
	 }
	 
	 @DeleteMapping(value="deleteUser/{id}")
	 public String deleteUser(@PathVariable("id")int id) {
		 return userService.deleteUserInfo(id);
	 }
	 
	 @PatchMapping(value="updateUser")
	 public String updateAdmin(@RequestBody User u) {
		 return userService.updateUserInfo(u);
	 }
	 
}
