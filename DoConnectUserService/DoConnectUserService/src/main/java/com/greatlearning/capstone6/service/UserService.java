package com.greatlearning.capstone6.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.greatlearning.capstone6.bean.User;
import com.greatlearning.capstone6.dao.UserDao;



@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public List<User> getAllUserInfo(){
	  	  return userDao.findAll();
	}
	    
	public String storeUserInfo(User u) {
	if(userDao.existsById(u.getId())) {
	  		  return "enter valid id";
	}else {
	  		userDao.save(u);
	  		  return "user information saved successfully";
	      }
	}
	
	public String deleteUserInfo(int id) {
		if(!userDao.existsById(id)) {
			return "id not present";
		}else {
			userDao.deleteById(id);
			return "id deleted successfully";
		}
	}
	
	public String updateUserInfo(User u) {
		if(!userDao.existsById(u.getId())) {
			return "email id not present";
		}else {
			User user=userDao.getById(u.getId());
			user.setPassword(u.getPassword());
			userDao.saveAndFlush(user);
			return "password updated successfully";
		}
	}
//	public String logoutInfo(String email) {
//		if(!adminDao.existsById(email)) {
//			return "email not present";
//		}else {
//			return "logged out successfully";
//		}
//	}
	
}
