package com.greatlearning.capstone6.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.capstone6.bean.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer>{

}
