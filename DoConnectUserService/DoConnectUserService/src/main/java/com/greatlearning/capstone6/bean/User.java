package com.greatlearning.capstone6.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
@Id
@Column(name="Id")
private int id;
@Column(name="Email")
private String email;
@Column(name="Password")
private String password;

public User() {
	super();
	// TODO Auto-generated constructor stub
}

public User(int id, String email, String password) {
	super();
	this.id = id;
	this.email = email;
	this.password = password;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
	return "User [id=" + id + ", email=" + email + ", password=" + password + "]";
}


}
