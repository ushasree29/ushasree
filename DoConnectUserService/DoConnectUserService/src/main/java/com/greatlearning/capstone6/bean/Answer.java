package com.greatlearning.capstone6.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="answer")
public class Answer {
@Id
@Column(name="Id")
private int id;
@Column(name="AName")
private String aname;
@Column(name="UId")
private int uid;
@Column(name="QId")
private int qid;

public Answer() {
	super();
	// TODO Auto-generated constructor stub
}

public Answer(int id, String aname, int uid, int qid) {
	super();
	this.id = id;
	this.aname = aname;
	this.uid = uid;
	this.qid = qid;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getAname() {
	return aname;
}
public void setAname(String aname) {
	this.aname = aname;
}
public int getUid() {
	return uid;
}
public void setUid(int uid) {
	this.uid = uid;
}
public int getQid() {
	return qid;
}
public void setQid(int qid) {
	this.qid = qid;
}

@Override
public String toString() {
	return "Answer [id=" + id + ", aname=" + aname + ", uid=" + uid + ", qid=" + qid + "]";
}



}
