package com.greatlearning.capstone6.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="question")
public class Question {
@Id
@Column(name="Id")
private int id;
@Column(name="QName")
private String qname;
@Column(name="UId")
private int uid;
public Question() {
	super();
	// TODO Auto-generated constructor stub
}

public Question(int id, String qname, int uid) {
	super();
	this.id = id;
	this.qname = qname;
	this.uid = uid;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getQname() {
	return qname;
}
public void setQname(String qname) {
	this.qname = qname;
}
public int getUid() {
	return uid;
}
public void setUid(int uid) {
	this.uid = uid;
}

@Override
public String toString() {
	return "Question [id=" + id + ", qname=" + qname + ", uid=" + uid + "]";
}



}
